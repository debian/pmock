"""pMock: Python mock object framework.

pMock provides support for creating mock objects for use in unit testing.
The api is modelled on the jmock mock object framework.

pMock is not under active development.
"""

classifiers = """\
Development Status :: 3 - Alpha
Intended Audience :: Developers
License :: OSI Approved :: Python Software Foundation License
Programming Language :: Python
Topic :: Software Development :: Libraries :: Python Modules
Topic :: Software Development :: Testing
"""

from setuptools import setup

doclines = __doc__.split("\n")

setup (
    name = "pmock",
    version = "0.3.1",
    maintainer="Graham Carlyle",
    maintainer_email="graham@grahamcarlyle.com",
    license="Same terms as Python",
    url = "https://bitbucket.org/grahamcarlyle/pmock-old",
    description = doclines[0],
    classifiers = filter(None, classifiers.split("\n")),
    long_description = "\n".join(doclines[2:]),
    package_dir = {"": "src"},
    py_modules = ["pmock"],
    tests_require=["nose"],
    test_suite="nose.collector"
    )
